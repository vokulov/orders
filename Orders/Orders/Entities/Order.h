//
//  Order.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer, Good,PlaceOfMeeting;

@interface Order : NSManagedObject

@property (nonatomic, retain) NSDate * dateAndTime;
@property (nonatomic, retain) NSSet *goods;
@property (nonatomic, retain) Customer *customer;
@property (nonatomic, retain) PlaceOfMeeting *placeForMeeting;

- (NSInteger) price;

@end

@interface Order (CoreDataGeneratedAccessors)

- (void)addGoodsObject:(Good *)value;
- (void)removeGoodsObject:(Good *)value;
- (void)addGoods:(NSSet *)values;
- (void)removeGoods:(NSSet *)values;

@end
