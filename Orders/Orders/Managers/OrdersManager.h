//
//  OrdersManager.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Order, Customer, Good, PlaceOfMeeting;

@interface OrdersManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedInstance;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (Order*) newOrder;
- (Customer*) newCustomer;
- (Good*) newGood;
- (PlaceOfMeeting*) newPlace;


- (NSArray*) loadAllOrders;
- (NSArray*) loadAllGoods;
- (NSArray*) loadAllCustomers;
- (NSArray*) loadAllPlaces;

@end
