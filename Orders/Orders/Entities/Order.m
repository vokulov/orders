//
//  Order.m
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "Order.h"
#import "Customer.h"
#import "Good.h"

@implementation Order

@dynamic dateAndTime;
@dynamic goods;
@dynamic customer;
@dynamic placeForMeeting;


- (NSInteger) price {
    NSInteger result = 0;
    for (Good* good in self.goods) {
        result += [good.price integerValue];
    }
    return result;
}

@end
