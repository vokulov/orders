//
//  PlaceOfMeeting.m
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "PlaceOfMeeting.h"


@implementation PlaceOfMeeting

@dynamic address;
@dynamic longitude;
@dynamic latitude;


@end
