//
//  PlaceOfMeeting.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PlaceOfMeeting : NSManagedObject

@property (nonatomic, retain)  NSString * address;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * latitude;


@end
