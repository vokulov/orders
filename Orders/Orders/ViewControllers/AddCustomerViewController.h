//
//  AddCustomerViewController.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCustomerViewController : UIViewController

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
