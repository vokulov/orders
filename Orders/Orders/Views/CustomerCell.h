//
//  CustomerCell.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *customerPhoto;
@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UILabel *orderCount;
@property (weak, nonatomic) IBOutlet UILabel *dateOfLastOrder;

@end
