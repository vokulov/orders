//
//  AddOrderViewController.h
//  Orders
//
//  Created by Constantine Ivanov on 2/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Order;

@protocol AddOrderDelegate <NSObject>

- (void) orderAdded:(Order*) order;

@end

@interface AddOrderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UILabel *placeOfMeetingLabel;
@property (weak,nonatomic) id<AddOrderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)changeDate:(id)sender;

@end
